# Installation

dans le répertoire de l'application effectuer la commande `yarn install` (avec [Yarn](https://yarnpkg.com/en/docs/install))

# Consigne

Le but de cet exercice est de rédiger l'algorithme de la fonction `sum`contenue dans le fichier `sum.js`

Une fois la méthode implémentée, son fonctionnement pourra être validé à l'aide de la commande `npm test` ou `yarn test`

## Résultat attendu

*Test Suites:* 1 passed, 1 total<br>
*Tests:*       4 passed, 4 total<br>
*Snapshots:*   0 total<br>
*Time:*        1.68s<br>
Ran all test suites.<br>
✨  Done in 2.95s.<br>